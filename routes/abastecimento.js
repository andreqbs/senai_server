import express from "express";
import {
    createAbastecimento,
    deleteAbastecimento,
    getAbastecimento,
    getAbastecimentoId
} from "../controllers/abastecimento.js";

const router = express.Router();

router.get('/', getAbastecimento);
router.get('/:id', getAbastecimentoId);
router.post('/', createAbastecimento);
router.delete('/:id', deleteAbastecimento);

export default router;

