import express from "express";
import {createCars, deleteCar, getCarId, getCars} from "../controllers/cars.js";

const router = express.Router();

router.get('/', getCars);
router.get('/:id', getCarId);
router.post('/', createCars);
router.delete('/:id', deleteCar);

export default router;

