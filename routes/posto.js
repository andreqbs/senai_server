import express from "express";
import {createPosto, getPostoId, getPostos, deletePosto} from "../controllers/posto.js";

const router = express.Router();

router.get('/', getPostos);
router.get('/:id', getPostoId);
router.post('/', createPosto);
router.delete('/:id', deletePosto);

export default router;

