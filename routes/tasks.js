import express from "express";
import {createTasks, deleteTask, getTaskId, getTasks} from "../controllers/tasks.js";

const router = express.Router();

router.get('/', getTasks);
router.get('/:id', getTaskId);
router.post('/', createTasks);
router.delete('/:id', deleteTask);

export default router;

