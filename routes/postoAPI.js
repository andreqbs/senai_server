import express from "express";
import {getPostos} from "../controllers/postoAPI.js";

const router = express.Router();

router.get('/', getPostos);

export default router;

