import express from "express";
import {createUser, getUser, getUserId} from "../controllers/users.js";

const router = express.Router();

router.get('/', getUser);
router.get('/:id', getUserId);
router.post('/', createUser);


// router.delete('/:id', async (req, res) => {
//    let conn
//    try {
//       const id = req.params.id
//       conn = await pool.getConnection()
//
//       let sql = `DELETE FROM users WHERE ID = ${id}`;
//       await conn.query(sql);
//       res.send('Deletado com sucesso');
//       conn.end();//fecha a conexão
//
//    } catch (error) {
//       throw error
//    } finally {
//       if (conn) {
//          conn.release()
//       }
//    }
//    res.send(`ERROR`);
// });

export default router;