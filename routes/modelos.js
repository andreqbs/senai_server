import express from "express";
import {createModelo, getModeloId, getModelos, deleteModelo} from "../controllers/modelos.js";

const router = express.Router();

router.get('/', getModelos);
router.get('/:id', getModeloId);
router.post('/', createModelo);
router.delete('/:id', deleteModelo);

export default router;

