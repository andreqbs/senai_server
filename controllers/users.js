import dbPool from '../db.js';
const pool = dbPool;

export const getUser = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT * FROM users`;
        let result = await conn.query(sql);
        await conn.end();
        res.send(result)
    } catch (error) {
        res.send("Erro na busca");
        throw error
    } finally {
        if (conn) {
            await conn.release()
        }
    }
}

export const getUserId = async (req, res) => {
    let conn
    try {
        const id = req.params.id
        conn = await pool.getConnection()

        let sql = `SELECT * FROM users WHERE ID = ${id}`;
        let result = await conn.query(sql);
        await conn.end();//fecha a conexão
        res.send(result)

    } catch (error) {
        res.send("Erro na busca");
        throw error
    } finally {
        if (conn) {
            await conn.release()
        }
    }
}

export const createUser = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `INSERT INTO users(nome, idade) VALUES ('${req.body.nome}','${req.body.idade}')`;

        await conn.query(sql, (err, res)=> {
            if(err) throw err;
            conn.end();//fecha a conexão
            res.send('OK')
        });
    } catch (error) {
        throw error
        res.send(`ERROR`);
    } finally {
        if (conn) {
            await conn.release()
        }
    }
}