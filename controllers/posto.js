import dbPool from '../db.js';
const pool = dbPool;

export const getPostos = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT posto_combustivel.id, posto_combustivel.CNPJ, posto_combustivel.fantasia, 
                    posto_combustivel.logradouro, posto_combustivel.numero, posto_combustivel.municipio,
                    posto_combustivel.uf 
                    FROM posto_combustivel`
        console.log(sql)
        let result = await conn.query(sql)

        res.send(result)
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const createPosto = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        console.log(req.body)

        let sql = `INSERT INTO posto_combustivel(CNPJ, fantasia, logradouro, numero, municipio, uf) VALUES 
            ('${req.body.cnpj}','${req.body.fantasia}','${req.body.logradouro}',
             '${req.body.numero}','${req.body.municipio}', '${req.body.uf}')`;

        await conn.query(sql, (err, res)=> {
            if(err) throw err;
            conn.end();//fecha a conexão
        });
        res.send('OK')
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const createPostoApi = async (data) => {
    let conn
    try {
        conn = await pool.getConnection()

        console.log(data.cnpj)

        let sql = `INSERT INTO posto_combustivel(CNPJ, fantasia, logradouro, numero, municipio, uf) VALUES 
            ('${data.cnpj}','${data.fantasia}','${data.logradouro}',
             '${data.numero}','${data.municipio}', '${data.uf}')`;

        await conn.query(sql, (err, res)=> {
            if(err) throw err;
            conn.end();//fecha a conexão
            res.send('OK');
        });
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const deletePosto = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `DELETE FROM posto_combustivel WHERE id = '${req.params.id}'`;
        await conn.query(sql,(err, res) => {
            if (err) throw err;
        });
        res.send("OK");
    }
    catch (error) {
        throw error
    }
}

export const getPostoId = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()
        let sql = `SELECT * FROM posto_combustivel WHERE id = '${req.params.id}'`;
        let result = await conn.query(sql);
        res.send(result);
    }
    catch (error) {
        throw error
    }
}