import fetch from "node-fetch";
import {compileETag} from "express/lib/utils.js";

export const getPostos = async (req, res) => {
    const API_POSTO = "https://receitaws.com.br/v1/cnpj/";
    try {
        // console.log('CNPJ' + req.body.cnpj)
        const response = await fetch(API_POSTO + req.body.cnpj);
        const data = await response.json();
        console.log({ data })
        return data;
    }
    catch (e) {
        console.log(e)
    }
}

export const createPosto = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        // console.log(req.body)

        let sql = `INSERT INTO posto_combustivel(CNPJ, fantasia, logradouro, numero, municipio, uf) VALUES 
            ('${req.body.cnpj}','${req.body.fantasia}','${req.body.logradouro}',
             '${req.body.numero}','${req.body.municipio}', '${req.body.uf}')`;

        await conn.query(sql, (err, res)=> {
            if(err) throw err;
            conn.end();//fecha a conexão
        });
        res.send('OK')
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}