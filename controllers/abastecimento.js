import dbPool from '../db.js';
import {getPostos} from "./postoAPI.js";
import {createPosto, createPostoApi} from "./posto.js";
const pool = dbPool;

export const getAbastecimento = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT lancamento_abastecimento.id, lancamento_abastecimento.data, 
                    lancamento_abastecimento.volume, lancamento_abastecimento.hodometro,
                    lancamento_abastecimento.valor_litro, lancamento_abastecimento.veiculo_id 
                    FROM lancamento_abastecimento`
        // console.log(sql)
        let result = await conn.query(sql)

        res.send(result)
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const createAbastecimento = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        const posto = await getPostos(req);
        console.log(posto);

        createPostoApi(posto)

        let sql = `INSERT INTO lancamento_abastecimento(data, volume,
                    hodometro, valor_litro, veiculo_id) VALUES 
            ('${req.body.data}','${req.body.volume}','${req.body.hodometro}','${req.body.valorLitro}',
             '${req.body.veiculoId}')`;

        await conn.query(sql, (err, res)=> {
            if(err) throw err;
            conn.end();//fecha a conexão
        });
        res.send('OK')
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const deleteAbastecimento = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `DELETE FROM lancamento_abastecimento WHERE id = '${req.params.id}'`;
        await conn.query(sql,(err, res) => {
            if (err) throw err;
        });
        res.send("OK");
    }
    catch (error) {
        throw error
    }
}

export const getAbastecimentoId = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()
        let sql = `SELECT * FROM lancamento_abastecimento WHERE id = '${req.params.id}'`;
        let result = await conn.query(sql);
        res.send(result);
    }
    catch (error) {
        throw error
    }
}