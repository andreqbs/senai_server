import dbPool from '../db.js';
const pool = dbPool;

export const getCars = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT veiculo.id, veiculo.placa, veiculo.volume_reservatorio, veiculo.ano_fabricacao,
                    veiculo.ano_modelo, veiculo.hodometro_inicial, modelo_veiculo.nome 
                    FROM veiculo 
                    INNER JOIN modelo_veiculo ON veiculo.modelo_veiculo_id = modelo_veiculo.id`
        console.log(sql)
        let result = await conn.query(sql)

        res.send(result)
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const createCars = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        console.log(req.body)

        let sql = `INSERT INTO veiculo(placa, volume_reservatorio,
                    ano_fabricacao,ano_modelo, hodometro_inicial, modelo_veiculo_id) VALUES 
            ('${req.body.placa}','${req.body.volume}','${req.body.anoFabricacao}','${req.body.anoModelo}',
             '${req.body.hodometroInicial}','${req.body.modeloVeiculoId}')`;

        await conn.query(sql, (err, res)=> {
            if(err) throw err;
            conn.end();//fecha a conexão
        });
        res.send('OK')
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const deleteCar = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `DELETE FROM veiculo WHERE id = '${req.params.id}'`;
        await conn.query(sql,(err, res) => {
            if (err) throw err;
        });
        res.send("OK");
    }
    catch (error) {
        throw error
    }
}

export const getCarId = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()
        let sql = `SELECT * FROM veiculo WHERE id = '${req.params.id}'`;
        let result = await conn.query(sql);
        res.send(result);
    }
    catch (error) {
        throw error
    }
}