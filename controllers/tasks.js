import dbPool from '../db.js';
const pool = dbPool;

export const getTasks = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT * FROM tarefas`
        let result = await conn.query(sql)

        res.send(result)
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const createTasks = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        console.log(req.body)

        let sql = `INSERT INTO tarefas(nome, hora) VALUES ('${req.body.nome}','${req.body.hora}')`;

        await conn.query(sql, (err, res)=> {
            if(err) throw err;
            conn.end();//fecha a conexão
        });
        res.send('OK')
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const deleteTask = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `DELETE FROM tarefas WHERE id = '${req.params.id}'`;
        await conn.query(sql,(err, res) => {
            if (err) throw err;
        });
        res.send("OK");
    }
    catch (error) {
        throw error
    }
}

export const getTaskId = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()
        let sql = `SELECT * FROM tarefas WHERE id = '${req.params.id}'`;
        let result = await conn.query(sql);
        res.send(result);
    }
    catch (error) {
        throw error
    }
}