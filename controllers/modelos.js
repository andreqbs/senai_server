import dbPool from '../db.js';
const pool = dbPool;

export const getModelos = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT modelo_veiculo.id, modelo_veiculo.nome, modelo_veiculo.fabricante
                    FROM 
                    modelo_veiculo`
        console.log(sql)
        let result = await conn.query(sql)

        res.send(result)
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const createModelo = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        console.log(req.body)

        let sql = `INSERT INTO modelo_veiculo(nome, fabricante) VALUES 
            ('${req.body.nome}','${req.body.fabricante}')`;

        await conn.query(sql, (err, res)=> {
            if(err) throw err;
            conn.end();//fecha a conexão
        });
        res.send('OK')
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
}

export const deleteModelo = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `DELETE FROM modelo_veiculo WHERE id = '${req.params.id}'`;
        await conn.query(sql,(err, res) => {
            if (err) throw err;
        });
        res.send("OK");
    }
    catch (error) {
        throw error
    }
}

export const getModeloId = async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()
        let sql = `SELECT * FROM modelo_veiculo WHERE id = '${req.params.id}'`;
        let result = await conn.query(sql);
        res.send(result);
    }
    catch (error) {
        throw error
    }
}