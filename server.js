import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import dbPool from './db.js';

import usersRoutes from './routes/users.js';
import tasksRoutes from './routes/tasks.js';
import carsRoutes from './routes/cars.js';
import modelosRoutes from './routes/modelos.js';
import abastecimentoRoutes from "./routes/abastecimento.js";
import postoRoutes from "./routes/posto.js";
import postoAPIRoutes from "./routes/postoAPI.js";

const app = express()
const PORT = process.env.PORT || 8080
const pool = dbPool;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use("/users", usersRoutes);
app.use("/tasks", tasksRoutes);
app.use("/cars", carsRoutes);
app.use("/modelos", modelosRoutes);
app.use("/abastecimentos", abastecimentoRoutes);
app.use("/postos", postoRoutes);
app.use("/postosapi", postoAPIRoutes);

//Route inicial
app.get('/', async (req, res) => {
    res.json({
        message: 'Funcionando :D'
    })
})

app.get('/dashpostos', async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT posto_combustivel.id as total FROM posto_combustivel`
        console.log(sql)
        let result = await conn.query(sql)

        res.send(result);
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
});

app.get('/dashtotalgasto', async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT (SUM(lancamento_abastecimento.volume)*SUM(lancamento_abastecimento.valor_litro)) as total
                    FROM lancamento_abastecimento`
        console.log(sql)
        let result = await conn.query(sql)

        res.send(result)
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
});

app.get('/dashbastecimentos', async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT lancamento_abastecimento.id as total FROM lancamento_abastecimento`
        console.log(sql)
        let result = await conn.query(sql)

        res.send(result);
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
});

app.get('/dashlitros', async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT SUM(volume) as total FROM lancamento_abastecimento`
        console.log(sql)
        let result = await conn.query(sql)

        res.send(result);
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
});

app.get('/dashveiculos', async (req, res) => {
    let conn
    try {
        conn = await pool.getConnection()

        let sql = `SELECT veiculo.id as total FROM veiculo`
        console.log(sql)
        let result = await conn.query(sql)

        res.send(result);
    } catch (error) {
        throw error
    } finally {
        if (conn) {
            conn.release()
        }
    }
});


app.all("*", (req, res) =>res.send("Rota errada."));

// Listening Server
app.listen(PORT, () => {
    console.log(`Server OK:${PORT}`)
})
